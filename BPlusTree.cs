﻿namespace SemestrWork
{
    internal class BPlusTree<T>
    {
        readonly SpecialInfoPairComparer DictionaryPairComparer = new SpecialInfoPairComparer();
        readonly InfoPairKeyComparer PairKeyComparer = new InfoPairKeyComparer();
        readonly int TreeMaxDegree;

        InternalNode? root;
        LeafNode? firstLeaf;
        public bool IsEmpty => firstLeaf == null;

        public BPlusTree(int m)
        {
            TreeMaxDegree = m;
        }

        /// <summary>
        /// Inserts <paramref name="value"/> with <paramref name="key"/>
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Insert(int key, T value)
        {
            if (IsEmpty)
            {
                LeafNode ln = new LeafNode(TreeMaxDegree, new InfoPair(key, value));
                firstLeaf = ln;
            }
            else
            {
                var leafNode = root == null ? firstLeaf : FindLeafNode(key);
    
                if (!leafNode!.Insert(new InfoPair(key, value)))
                {
                    leafNode.DictionaryInfo[leafNode.InfoCount] = new InfoPair(key, value);
                    leafNode.InfoCount++;
                    SortInfo(leafNode.DictionaryInfo!);
    
                    int midpoint = (int)Math.Ceiling((TreeMaxDegree + 1) / 2.0) - 1;
                    InfoPair[] halfDictionary = SplitDictionaryInfo(leafNode, midpoint);
    
                    if (leafNode.parent == null)
                    {
                        var parentKeys = new int?[TreeMaxDegree];
                        parentKeys[0] = halfDictionary[0].Key;
                        var parent = new InternalNode(TreeMaxDegree, parentKeys);
                        leafNode.parent = parent;
                        parent.AppendChildPointer(leafNode);
                    }
                    else
                    {
                        var newParentKey = halfDictionary[0].Key;
                        leafNode.parent.Keys[leafNode.parent.Degree - 1] = newParentKey;
                        Array.Sort(leafNode.parent.Keys, 0, leafNode.parent.Degree);
                    }
    
                    var newLeafNode = new LeafNode(TreeMaxDegree, halfDictionary, leafNode.parent);
    
                    int pointerIndex = leafNode.parent.FindIndexOfPointer(leafNode) + 1;
                    leafNode.parent.InsertChildPointer(newLeafNode, pointerIndex);
    
                    newLeafNode.Right = leafNode.Right;
                    if (newLeafNode.Right != null)
                        newLeafNode.Right.Left = newLeafNode;
                    leafNode.Right = newLeafNode;
                    newLeafNode.Left = leafNode;
    
                    if (root == null)
                        root = leafNode.parent;
                    else
                    {
                        var internalNode = leafNode.parent;
                        while (internalNode != null) 
                        {
                            if (internalNode.IsOverfull)
                                SplitInternalNode(internalNode);
                            else
                                break;
                            internalNode = internalNode.parent;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Checks if key is presented.
        /// </summary>
        /// <param name="key"></param>
        /// <returns>
        /// Pair of result and value if result is true
        /// </returns>
        public (bool,T?) Contains(int key)
        {   
            if (IsEmpty)
                return (false, default);
    
            var leafNode = (root == null) ? firstLeaf : FindLeafNode(key);
    
            var pairs = leafNode!.DictionaryInfo;
            int index = BinarySearch(pairs!, leafNode.InfoCount, key);
            if (index < 0)
                return (false, default);
            else
                return (true, pairs[index]!.Value);
        }
    
        /// <summary>
        /// Traverses Tree and do <paramref name="action"/> with each element
        /// </summary>
        /// <param name="action"></param>
        public void Traverse(Action<T> action)
        {
            if(IsEmpty) return;
            var current = firstLeaf;
            while(current!.Left != null)
                current = current.Left;
            while(current != null)
            {
                for (int i = 0; i < current.InfoCount; i++)
                    action(current.DictionaryInfo[i]!.Value);
                current = current.Right;
            }
        }

        /// <summary>
        /// Removes element connected with <paramref name="key"/>
        /// </summary>
        /// <param name="key"></param>
        /// <returns>
        /// Value
        /// </returns>
        /// <exception cref="NotImplementedException"></exception>
        public T Remove(int key)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Search for key in diaposone
        /// </summary>
        /// <param name="dps"></param>
        /// <param name="numPairs"></param>
        /// <param name="t"></param>
        /// <returns> key </returns>
        private int BinarySearch(InfoPair[] dps, int numPairs, int t)
        {
            return Array.BinarySearch(dps, 0, numPairs, new InfoPair(t, default), PairKeyComparer);
        }

        /// <summary>
        /// Searches for LeafNode
        /// </summary>
        /// <param name="key"></param>
        /// <returns>
        /// LeafNode connected with <paramref name="key"/>
        /// </returns>
        private LeafNode FindLeafNode(int key)
        {
            var keys = root!.Keys;
            int i;

            for (i = 0; i < root.Degree - 1; i++)
            {
                if (key < keys[i])
                    break;
            }

            var child = root.ChildrenPointers[i];
            if (child is LeafNode)
            {
                return child as LeafNode;
            }
            else
            {
                return FindLeafNode(child as InternalNode, key);
            }
        }

        /// <summary>
        /// Searches leafNode in tree
        /// </summary>
        /// <param name="internalNode"></param>
        /// <param name="key"></param>
        /// <returns>LeafNode connected with <paramref name="key"/> whose parrent (grandparent) is <paramref name="internalNode"/></returns>
        private LeafNode FindLeafNode(InternalNode internalNode, int key)
        {

            var keys = internalNode.Keys;
            int i;

            for (i = 0; i < internalNode.Degree - 1; i++)
            {
                if (key.CompareTo(keys[i]) < 0)
                {
                    break;
                }
            }
            var childNode = internalNode.ChildrenPointers[i];
            if (childNode is LeafNode)
            {
                return childNode as LeafNode;
            }
            else
            {
                return FindLeafNode(internalNode.ChildrenPointers[i] as InternalNode, key);
            }
        }

        /// <summary>
        /// Balance Tree
        /// </summary>
        /// <param name="internalNode"></param>
        private void HandleDeficiency(InternalNode internalNode)
        {
            InternalNode sibling;
            var parent = internalNode.parent;
            if (root == internalNode)
            {
                for (int i = 0; i < internalNode.ChildrenPointers.Length; i++)
                {
                    if (internalNode.ChildrenPointers[i] != null)
                    {
                        if (internalNode.ChildrenPointers[i] is InternalNode)
                        {
                            root = internalNode.ChildrenPointers[i] as InternalNode;
                            root!.parent = null;
                        }
                        else if (internalNode.ChildrenPointers[i] is LeafNode)
                            root = null;
                    }
                }
            }
            else if (internalNode.LeftSibling != null && internalNode.LeftSibling.IsLendable)
            {
                sibling = internalNode.LeftSibling;
            }
            else if (internalNode.RightSibling != null && internalNode.RightSibling.IsLendable)
            {
                sibling = internalNode.RightSibling;

                var borrowedKey = sibling.Keys[0];
                var pointer = sibling.ChildrenPointers[0];

                internalNode.Keys[internalNode.Degree - 1] = parent.Keys[0];
                internalNode.ChildrenPointers[internalNode.Degree] = pointer;

                parent.Keys[0] = borrowedKey;

                sibling.RemovePointer(0);
                Array.Sort(sibling.Keys);
                sibling.RemovePointer(0);
                internalNode.ChildrenPointers = ShiftDown(internalNode.ChildrenPointers!, 1);
            }
            else if (internalNode.RightSibling != null && internalNode.RightSibling.IsMergeable)
            {
                sibling = internalNode.RightSibling;
                sibling.Keys[sibling.Degree - 1] = parent.Keys[parent.Degree - 2];
                Array.Sort(sibling.Keys, 0, sibling.Degree);
                parent.Keys[parent.Degree - 2] = null;

                for (int i = 0; i < internalNode.ChildrenPointers.Length; i++)
                {
                    if (internalNode.ChildrenPointers[i] != null)
                    {
                        sibling.PrependChildPointer(internalNode.ChildrenPointers[i]!);
                        internalNode.ChildrenPointers[i]!.parent = sibling;
                        internalNode.RemovePointer(i);
                    }
                }

                parent.RemovePointer(internalNode);

                sibling.LeftSibling = internalNode.LeftSibling;
            }

            if (parent != null && parent.IsDeficient)
                HandleDeficiency(parent);
        }
        /// <summary>
        /// Moves <paramref name="pointers"/> values to left according to <paramref name="amount"/>
        /// </summary>
        /// <param name="pointers"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        private Node[] ShiftDown(Node[] pointers, int amount)
        {
            var newPointers = new Node[TreeMaxDegree + 1];
            for (int i = amount; i < pointers.Length; i++)
            {
                newPointers[i - amount] = pointers[i];
            }
            return newPointers;
        }

        /// <summary>
        /// Searches for first occurence of null in <paramref name="dps"/>
        /// </summary>
        /// <param name="dps"></param>
        /// <returns>index or -1</returns>
        private static int LinearNullSearch(InfoPair[] dps)
        {
            for (int i = 0; i < dps.Length; i++)
            {
                if (dps[i] == null)
                {
                    return i;
                }
            }
            return -1;
        }

        /// <summary>
        /// Searches for first occurence of null in <paramref name="pointers"/>
        /// </summary>
        /// <param name="pointers"></param>
        /// <returns>index or -1</returns>
        private static int LinearNullSearch(Node[] pointers)
        {
            for (int i = 0; i < pointers.Length; i++)
            {
                if (pointers[i] == null)
                {
                    return i;
                }
            }
            return -1;
        }

        private void SortInfo(InfoPair[] dictionary)
        {
            Array.Sort(dictionary, DictionaryPairComparer);
        }

        /// <summary>
        /// Splites <paramref name="internalNode"/> pointers
        /// </summary>
        /// <param name="internalNode"></param>
        /// <param name="split"></param>
        /// <returns></returns>
        private Node[] SplitChildPointers(InternalNode internalNode, int split)
        {
            var pointers = internalNode.ChildrenPointers;
            var halfPointers = new Node[TreeMaxDegree + 1];

            for (int i = split + 1; i < pointers.Length; i++)
            {
                halfPointers[i - split - 1] = pointers[i]!;
                internalNode.RemovePointer(i);
            }

            return halfPointers;
        }

        private InfoPair[] SplitDictionaryInfo(LeafNode leafNode, int split)
        {

            var dictionary = leafNode.DictionaryInfo!;

            InfoPair[] halfDict = new InfoPair[TreeMaxDegree];

            for (int i = split; i < dictionary.Length; i++)
            {
                halfDict[i - split] = dictionary[i]!;
                leafNode.Delete(i);
            }

            return halfDict;
        }

        private void SplitInternalNode(InternalNode internalNode)
        {
            var parent = internalNode.parent;

            int midpoint = (int)Math.Ceiling((TreeMaxDegree + 1) / 2.0) - 1;
            var newParentKey = internalNode.Keys[midpoint];
            var halfKeys = SplitKeys(internalNode.Keys, midpoint);
            Node[] halfPointers = SplitChildPointers(internalNode, midpoint);

            internalNode.Degree = LinearNullSearch(internalNode.ChildrenPointers);

            var sibling = new InternalNode(TreeMaxDegree, halfKeys, halfPointers);
            foreach (var pointer in halfPointers)
            {
                if (pointer != null)
                    pointer.parent = sibling;
            }

            sibling.RightSibling = internalNode.RightSibling;
            if (sibling.RightSibling != null)
                sibling.RightSibling.LeftSibling = sibling;

            internalNode.RightSibling = sibling;
            sibling.LeftSibling = internalNode;

            if (parent == null)
            {
                var keys = new int?[TreeMaxDegree];
                keys[0] = newParentKey;
                var newRoot = new InternalNode(TreeMaxDegree, keys);
                newRoot.AppendChildPointer(internalNode);
                newRoot.AppendChildPointer(sibling);
                root = newRoot;
                internalNode.parent = newRoot;
                sibling.parent = newRoot;
            }
            else
            {
                parent.Keys[parent.Degree - 1] = newParentKey;
                Array.Sort(parent.Keys, 0, parent.Degree);

                int pointerIndex = parent.FindIndexOfPointer(internalNode) + 1;
                parent.InsertChildPointer(sibling, pointerIndex);
                sibling.parent = parent;
            }
        }

        private int?[] SplitKeys(int?[] keys, int split)
        {
            var halfKeys = new int?[TreeMaxDegree];
            keys[split] = null;
            for (int i = split + 1; i < keys.Length; i++)
            {
                halfKeys[i - split - 1] = keys[i];
                keys[i] = null;
            }

            return halfKeys;
        }

        abstract class Node
        {
            public InternalNode? parent;
            public abstract bool IsDeficient { get; }
            public abstract bool IsLendable { get; }
            public abstract bool IsMergeable { get; }
            public abstract bool HasNotEnough { get; }
        }

        class LeafNode : Node
        {
            readonly int maxNumPairs;
            readonly int minNumPairs;
            public int InfoCount;
            public LeafNode? Left;
            public LeafNode? Right;
            public InfoPair?[] DictionaryInfo;

            public override bool IsDeficient => InfoCount < minNumPairs;
            public override bool IsLendable => InfoCount > minNumPairs;
            public override bool IsMergeable => InfoCount == minNumPairs;
            public override bool HasNotEnough => InfoCount < maxNumPairs;

            public bool IsFull => InfoCount == maxNumPairs;

            public void Delete(int index)
            {
                DictionaryInfo[index] = null!;
                InfoCount--;
            }

            public bool Insert(InfoPair valuePair)
            {
                if (IsFull)
                    return false;
                else
                {
                    DictionaryInfo[InfoCount] = valuePair;
                    InfoCount++;
                    Array.Sort(DictionaryInfo, 0, InfoCount);

                    return true;
                }
            }

            public void UpdateKeys(int deleted)
            {
                var newMinimum = DictionaryInfo[0].Key;
                parent?.UpdateKeys(deleted, newMinimum);
                
            }

            public LeafNode(int m, InfoPair valuePair)
            {
                maxNumPairs = m - 1;
                minNumPairs = (int)(Math.Ceiling(m / 2.0) - 1);
                DictionaryInfo = new InfoPair[m];
                InfoCount = 0;
                Insert(valuePair);
            }

            public LeafNode(int m, InfoPair[] valuePairs, InternalNode parent)
            {
                maxNumPairs = m - 1;
                minNumPairs = (int)(Math.Ceiling(m / 2.0) - 1);
                DictionaryInfo = valuePairs;
                InfoCount = LinearNullSearch(valuePairs);
                this.parent = parent;
            }
        }

        class InternalNode: Node
        {
            readonly int maxDegree;
            readonly int minDegree;

            public int?[] Keys;
            public Node?[] ChildrenPointers;

            public int Degree { get; set; }
            public InternalNode? LeftSibling { get; set; }
            public InternalNode? RightSibling { get; set; }

            public override bool IsDeficient => Degree < minDegree;
            public override bool IsLendable => Degree > minDegree;
            public override bool IsMergeable => Degree == minDegree;
            public override bool HasNotEnough => Degree < minDegree;
            public bool IsOverfull => Degree == maxDegree + 1;

            public void UpdateKeys(int deleted, int newMinimum)
            {
                var index = Array.IndexOf(Keys, deleted,0, Degree);
                if (index == -1) return;
                Keys[index] = newMinimum;
                parent?.UpdateKeys(deleted, newMinimum);
            }

            public void AppendChildPointer(Node pointer)
            {
                ChildrenPointers[Degree] = pointer;
                Degree++;
            }

            public int FindIndexOfPointer(Node pointer)
            {
                for (int i = 0; i < ChildrenPointers.Length; i++)
                {
                    if (ChildrenPointers[i] == pointer)
                    {
                        return i;
                    }
                }
                return -1;
            }

            public void InsertChildPointer(Node pointer, int index)
            {
                for (int i = Degree - 1; i >= index; i--)
                {
                    ChildrenPointers[i + 1] = ChildrenPointers[i];
                }
                ChildrenPointers[index] = pointer;
                Degree++;
            }

            public void PrependChildPointer(Node pointer)
            {
                for (int i = Degree - 1; i >= 0; i--)
                {
                    ChildrenPointers[i + 1] = ChildrenPointers[i];
                }
                ChildrenPointers[0] = pointer;
                Degree++;
            }

            public void RemoveKey(int index)
            {
                Keys[index] = null;
            }

            public  void RemovePointer(int index)
            {
                ChildrenPointers[index] = null!;
                Degree--;
            }

            public void RemovePointer(Node pointer)
            {
                for (int i = 0; i < ChildrenPointers.Length; i++)
                {
                    if (ChildrenPointers[i] == pointer)
                    {
                        ChildrenPointers[i] = null!;
                    }
                }
                Degree--;
            }

            public InternalNode(int m, int?[] keys)
            {
                maxDegree = m;
                minDegree = (int)Math.Ceiling(m / 2.0);
                Degree = 0;
                Keys = keys;
                ChildrenPointers = new Node[this.maxDegree + 1];
            }

            public InternalNode(int m, int?[] keys, Node[] pointers)
            {
                maxDegree = m;
                minDegree = (int)Math.Ceiling(m / 2.0);
                Degree = LinearNullSearch(pointers);
                Keys = keys;
                ChildrenPointers = pointers;
            }

        }

        class InfoPair : IComparable<InfoPair>
        {
            public readonly int Key;
            public readonly T Value;

            public InfoPair(int key, T value)
            {
                Key = key;
                Value = value;
            }

            public int CompareTo(InfoPair? o)
            {
                if (Key == o?.Key)
                {
                    return 0;
                }
                else if (Key > o?.Key)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
        }

        class SpecialInfoPairComparer : IComparer<InfoPair>
        {
            public int Compare(InfoPair? o1, InfoPair? o2)
            {
                if (o1 == null && o2 == null)
                    return 0;
                else if (o1 == null)
                    return 1;
                else if (o2 == null)
                    return -1;
                return o1.CompareTo(o2);
            }
        }

        class InfoPairKeyComparer : IComparer<InfoPair>
        {
            public int Compare(InfoPair? a, InfoPair? b)
            {
                return a!.Key.CompareTo(b!.Key);
            }
        }
    }
}